# Taxonomy Term Advanced Sidebar

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation

INTRODUCTION
------------

This module provides Taxonomy Term Edit Page UI with 
sidebar option like Node Edit page.


INSTALLATION
------------

The installation of this module is like other Drupal modules.

 1. Copy/upload the term_sidebar module to the modules directory.

 2. Enable the 'term_sidebar' module in 'Extend'.
   (/admin/modules)

<?php

/**
 * @file
 * Taxomy term advanced sidebar module file.
 */

use Drupal\Core\Entity\Display\EntityFormDisplayInterface;
use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Implements hook_module_implements_alter().
 */
function term_sidebar_module_implements_alter(&$implementations, $hook) {
  // Added to modify weight of form_alter execution
  // We need this to duplicate actions buttons properly.
  if ($hook == 'form_alter') {
    $group = $implementations['term_sidebar'];
    unset($implementations['term_sidebar']);
    $implementations['term_sidebar'] = $group;
  }
}

/**
 * Implements hook_form_BASE_FORM_ID_alter() for \Drupal\node\NodeForm.
 *
 * Changes vertical tabs to container.
 */
function term_sidebar_form_alter(&$form, FormStateInterface $form_state) {
  if ($form_state->getFormObject() instanceof EntityFormInterface) {
    $form_display = $form_state->get('form_display');
    if ($form_display instanceof EntityFormDisplayInterface && $form_display->getMode() === 'delete') {
      return;
    }
    $entity = $form_state->getFormObject()->getEntity();
    $entityTypeId = $entity->getEntityTypeId();
    if ($entityTypeId == 'taxonomy_term') {
      $dateFormatter = \Drupal::service('date.formatter');
      $entityFieldManager = \Drupal::service('entity_field.manager');

      $bundle = $entity->bundle();
      $fields = $entityFieldManager->getFieldDefinitions($entityTypeId, $bundle);

      $form['main_wrapper'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => [
            'layout-form clearfix',
          ],
        ],
        '#weight' => -10,
      ];

      $form['main'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => [
            'layout-region layout-region--node-main entity-main layout-region--main',
          ],
        ],
        '#weight' => -10,
        '#group' => 'main_wrapper',
      ];

      $form['term_main']['#attributes']['class'][] = 'layout-region__content';
      $form['term_main']['#type'] = 'container';
      $form['term_main']['#group'] = 'main';
      foreach ($fields as $field_name => $field_definition) {
        if (isset($form[$field_name])) {
          $form[$field_name]['#group'] = 'term_main';
        }
      }

      $form['advanced_wrapper'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => [
            'layout-region layout-region--node-secondary layout-region--secondary',
          ],
        ],
        '#weight' => -9,
        '#group' => 'main_wrapper',
      ];
      $form['advanced']['#type'] = 'container';
      $form['advanced']['#attributes']['class'][] = 'layout-region__content entity-meta';
      $form['advanced']['#group'] = 'advanced_wrapper';
      $form['advanced']['#accordion'] = TRUE;
      $form['meta'] = [
        '#type' => 'container',
        '#group' => 'advanced',
        '#weight' => -10,
        '#title' => t('Status'),
        '#attributes' => ['class' => ['entity-meta__header']],
        '#tree' => TRUE,
        '#access' => TRUE,
      ];

      $form['meta']['published'] = [
        '#type' => 'item',
        '#markup' => $entity->isPublished() ? t('Published') : t('Not published'),
        '#access' => !$entity->isNew(),
        '#wrapper_attributes' => ['class' => ['entity-meta__title']],
      ];
      $form['meta']['changed'] = [
        '#type' => 'item',
        '#title' => t('Last saved'),
        '#markup' => !$entity->isNew() ? $dateFormatter->format($entity->getChangedTime(), 'short') : t('Not saved yet'),
        '#wrapper_attributes' => ['class' => ['entity-meta__last-saved']],
      ];

      if (isset($form['path'])) {
        $form['path']['#type'] = 'details';
        $form['path']['#title'] = $form['path']['widget']['#title'];

        $form['path']['#group'] = 'advanced';
      }
      if (isset($form['relations'])) {
        $form['relations']['#group'] = 'advanced';
      }
      if (isset($form['content_translation'])) {
        $form['content_translation']['#group'] = 'advanced';
      }

      $form['footer'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => [
            'layout-region layout-region--node-footer layout-region--footer',
          ],
        ],
        '#weight' => -8,
        '#group' => 'main_wrapper',
      ];

      $theme = \Drupal::service('theme.manager')->getActiveTheme()->getName();
      if (!in_array($theme, ['gin', 'claro'])) {
        $theme_extensions = \Drupal::service('theme.manager')->getActiveTheme()->getBaseThemeExtensions();
        if (is_array($theme_extensions)) {
          $theme = array_key_first($theme_extensions);
        }
      }

      switch ($theme) {
        case 'claro':
          $form['#attributes']['class'][] = 'layout-node-form';
          $form['term_footer']['#type'] = 'container';
          $form['term_footer']['#attributes']['class'][] = 'layout-region__content';
          $form['term_footer']['#group'] = 'footer';

          $form['status']['#group'] = 'term_footer';

          $form['actions']['#group'] = 'term_footer';
          $form['term_footer']['actions'] = $form['actions'];

          // Unset default action buttons.
          unset($form['actions']);

          $form['#attached']['library'][] = 'claro/node-form';
          $form['#attached']['library'][] = 'claro/form-two-columns';
          break;

        case 'gin':

          // Action buttons.
          if (isset($form['actions'])) {
            if (isset($form['actions']['preview'])) {
              // Put Save after Preview.
              $save_weight = $form['actions']['preview']['#weight'] ? $form['actions']['preview']['#weight'] + 1 : 11;
              $form['actions']['submit']['#weight'] = $save_weight;
            }

            // Move entity_save_and_addanother_node after preview.
            if (isset($form['actions']['entity_save_and_addanother_node'])) {
              // Put Save after Preview.
              $save_weight = $form['actions']['entity_save_and_addanother_node']['#weight'];
              $form['actions']['preview']['#weight'] = $save_weight - 1;
            }

            // Add sidebar toggle.
            $hidePanel = new TranslatableMarkup('Hide sidebar panel');
            $form['actions']['gin_sidebar_toggle'] = [
              '#markup' => '<a href="#toggle-sidebar" class="meta-sidebar__trigger trigger" role="button" title="' . $hidePanel . '" aria-controls="gin_sidebar"><span class="visually-hidden">' . $hidePanel . '</span></a>',
              '#weight' => '999',
            ];
            $form['#attached']['library'][] = 'gin/sidebar';

            // Create gin_actions group.
            $form['gin_actions'] = [
              '#type' => 'container',
              '#weight' => -1,
              '#multilingual' => TRUE,
              '#attributes' => [
                'class' => [
                  'gin-sticky',
                ],
              ],
            ];
            // Assign status to gin_actions.
            $form['status']['#group'] = 'gin_actions';

            // Move all actions over.
            $form['gin_actions']['actions'] = ($form['actions']) ?? [];
            $form['gin_actions']['actions']['#weight'] = 130;

            // Unset default action buttons.
            unset($form['actions']);
          }

          // Attach libraries.
          $form['#attached']['library'][] = 'claro/node-form';
          $form['#attached']['library'][] = 'gin/edit_form';

          // Add a class that allows the logic in gin's edit_form.js to identify the form as a node edit form even though it is not.
          $form['#attributes']['class'][] = 'gin-node-edit-form';

          $form['main_wrapper']['main']['#attributes'] = [
            'class' => [
              'layout-region layout-region-node-main layout-region--main',
            ],
          ];
          $form['advanced_wrapper']['#attributes'] = [
            'class' => [
              'layout-region layout-region-node-secondary',
            ],
          ];
          $form['footer']['#attributes'] = [
            'class' => [
              'layout-node-form__actions',
            ],
          ];
          $form['gin_actions']['#group'] = 'footer';
          $form['#attributes']['class'][] = 'page-wrapper__node-edit-form';
          break;

        default:
          break;
      }

    }
  }
}

/**
 * Implements hook_preprocess_html() for attach body glass.
 */
function term_sidebar_preprocess_html(&$variables) {
  $theme = \Drupal::service('theme.manager')->getActiveTheme()->getName();
  if (!in_array($theme, ['gin', 'claro'])) {
    $theme_extensions = \Drupal::service('theme.manager')->getActiveTheme()->getBaseThemeExtensions();
    if (is_array($theme_extensions)) {
      $theme = array_key_first($theme_extensions);
    }
  }

  switch ($theme) {
    // Edit form? Use the new Gin Edit form layout.
    case 'gin':
      $routes = [
        'entity.taxonomy_term.add_form',
        'entity.taxonomy_term.edit_form',
      ];
      $routeName = \Drupal::routeMatch()->getRouteName();
      if (in_array($routeName, $routes)) {
        $variables['attributes']['class'][] = 'gin--edit-form';
      }
      break;

    default:
      // code...
      break;
  }
}
